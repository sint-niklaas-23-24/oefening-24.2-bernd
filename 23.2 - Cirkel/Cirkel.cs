﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _23._2___Cirkel
{
    internal class Cirkel
    {
        //attributen
        double _straal;
        //constructor
        public Cirkel(int r)
        {
            Straal = r;
        }
        //properties
        public double Straal
        {  get { return _straal; } set {  _straal = value; } }
        //methodes
        public double BerekenOmtrek()
        {
            return Math.Round((Straal * 2) * Math.PI, 2); ;
        }
       public string FormattedOmtrek()
        {
            return Math.Round(BerekenOmtrek(),2).ToString();
        }
        public double BerekenOppervlakte()
        {
            return Math.PI * Math.Pow(Straal, 2);
        }
        public string FormattedOppervlakte()
        {
            return Math.Round(BerekenOppervlakte(), 2).ToString();
        }
        public override string ToString() 
        {
            return "Omtrek:".PadRight(30) + FormattedOmtrek() + Environment.NewLine + Environment.NewLine+ "Oppervlakte:".PadRight(30)+FormattedOppervlakte();
        }
    }
}
