﻿using System.Windows;

namespace _23._2___Cirkel
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

        }


        private void BtnBerekenen_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int straal = Convert.ToInt32(TxtStraal.Text);
                Cirkel nieuweCirkel = new Cirkel(straal);
                Lblresultaat.Content = nieuweCirkel.ToString();
            }
            catch (FormatException)
            {
                MessageBox.Show("U gaf geen numerieke waarde in!", "Foute invoer", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}